package qaclickacademy;


import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Driver;
import java.util.Properties;

import javax.xml.crypto.Data;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class SeleniumTest {
	WebDriver drive = null;

	@Test(priority=0)
	public void one() throws IOException {
		Properties pr = new Properties();
		FileInputStream fl = new FileInputStream("/home/thinkpad/eclipse-workspace/Testng/src/data.properties");
		pr.load(fl);
		if (pr.getProperty("browser").contains("firefox")) {
			System.setProperty("webdriver.gecko.driver", "/home/thinkpad/Downloads/geckodriver");
			drive = new FirefoxDriver();
		} else if (pr.getProperty("browser").equals("chrome")) {
			drive = new ChromeDriver();
		}
		drive.get(pr.getProperty("url"));
//		System.out.println("test one");
	}

	@Test(priority=1)
	public void login1() throws IOException {
		drive.findElement(By.id("username")).sendKeys("superadmin");
		drive.findElement(By.id("password")).sendKeys("super@adminQureka");
		drive.findElement(By.id("login-submit")).click();
		drive.close();
		one();
	}
	
	@Test(priority=2)
	public void loginError() {
		drive.findElement(By.id("username")).sendKeys("superadmin");
		drive.findElement(By.id("password")).sendKeys("sup@adminQureka");
		drive.findElement(By.id("login-submit")).click();
		
	}

}
